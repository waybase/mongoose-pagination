/* eslint-disable no-underscore-dangle */
export const validateCursor = (value) => {
    if (!value) {
        return value;
    }

    const buff = Buffer.from(value, 'base64');
    return JSON.parse(buff.toString('utf-8'));
};

export const generateCursor = (node, sort) => {
    let value = { _id: node._id };

    if (sort) {
        value = {
            ...value,
            sort: {
                field: sort.field,
                value: node[sort.field],
            },
        };
    }

    const buff = Buffer.from(JSON.stringify(value));
    return buff.toString('base64');
};
