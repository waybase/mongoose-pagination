// from https://github.com/codeinbrain/honey-pager/blob/master/lib/cursor-paginator.js
import drop from 'lodash/drop';
import dropRight from 'lodash/dropRight';
import head from 'lodash/first';
import tail from 'lodash/last';
import { validateCursor, generateCursor } from './helpers';

async function paginate(
    query = {},
    {
        all = false,
        after: argsAfter,
        before: argsBefore,
        fields = [],
        first,
        last,
        sort,
    } = {}
) {
    if (typeof first !== 'undefined' && typeof last !== 'undefined') {
        throw new Error('first and last cannot be set at the same time.');
    }

    let after;
    let before;

    if (argsAfter) {
        after = await validateCursor(argsAfter);
    }

    if (argsBefore) {
        before = await validateCursor(argsBefore);
    }

    const $and = [query];

    const cursor = this.find({
        $and,
    }).select(
        (fields.length && sort?.field ? [...fields, sort.field] : fields).join(
            ' '
        )
    );

    if (sort) {
        const { order } = sort;

        cursor.sort({
            [sort.field]: order,
            _id: order,
        });
    }

    if (all) {
        return cursor;
    }

    const limit = first || last || 10;
    const isDescSort = sort?.order === 'desc';
    let hasNextPage = false;
    let hasPreviousPage = false;

    const buildCursorCondition = (cursor, way) => {
        if (cursor.sort) {
            const {
                _id,
                sort: { field, value },
            } = cursor;

            return {
                $or: [
                    {
                        [field]: { [way]: value },
                    },
                    {
                        [field]: value,
                        _id: { [way]: _id },
                    },
                ],
            };
        }

        // eslint-disable-next-line no-underscore-dangle
        return { _id: { [way]: cursor._id } };
    };

    const totalCount = await this.countDocuments({
        $and,
    });

    if (after) {
        $and.push(buildCursorCondition(after, isDescSort ? '$lt' : '$gt'));

        hasPreviousPage = true;
    }

    if (before) {
        $and.push(buildCursorCondition(before, isDescSort ? '$gt' : '$lt'));

        hasNextPage = true;
    }

    const hasOffset = !(hasNextPage && hasPreviousPage);

    cursor.limit(limit + (hasOffset ? 1 : 0));

    if (last) {
        let offset = totalCount - last - (hasOffset ? 1 : 0);

        if (before) {
            const dataCount = await this.countDocuments($and);
            offset = dataCount - last - 1;
        }

        cursor.skip(offset < 0 ? 0 : offset);
    }

    let data = await cursor;

    if (hasOffset && data.length > limit) {
        if (first) {
            hasNextPage = true;
        }

        if (last) {
            hasPreviousPage = true;
        }

        if (first || after) {
            data = dropRight(data);
        } else if (last || before) {
            data = drop(data);
        }
    }

    const edges = data.map((v) => ({
        cursor: generateCursor(v, sort),
        node: v,
    }));

    return {
        totalCount,
        pageInfo: {
            hasNextPage,
            hasPreviousPage,
            startCursor: head(edges)?.cursor ?? null,
            endCursor: tail(edges)?.cursor ?? null,
        },
        edges,
    };
}

export const paginationPlugin = (schema) => {
    // eslint-disable-next-line no-param-reassign
    schema.statics.paginate = paginate;
};
